class UrlMappings {

	static mappings = {
		"/org/foa/callback" (controller : "callBack") {
			action = [ POST : 'post', PUT : 'put', GET : 'get', DELETE : 'delete' ]
		}

		"/org/foa/callbacks/bitpay" (controller : "callBack") {
			action = [ POST : 'post', PUT : 'put', GET : 'get', DELETE : 'delete' ]
		}

		"/org/foa/callbacks/coinbase" (controller : "callBack") {
			action = [ POST : 'post', PUT : 'put', GET : 'get', DELETE : 'delete' ]
		}

		"/org/foa/ping" {
			controller = "ping"
			action = "ping"
		}

		"/ping" {
			controller = "ping"
			action = "ping"
		}

		"/"(view:"/index")
		"500"(view:'/error')
	}
}
