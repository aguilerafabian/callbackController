package org.foa.callbacks

import com.gateway.util.MapUtil

/**
 * Controller para Callbacks
 *
 * @author faguilera
 */

class CallBackController {

	// Public methods

	def post = {
		doAction("POST")
	}

	def put = {
		doAction("PUT")
	}

	def get = {
		doAction("GET")
	}

	def delete = {
		doAction("DELETE")
	}

	// Implementation

	protected Map doAction(String action) {
		long t1 = System.currentTimeMillis()

		String logId = "" + System.currentTimeMillis()

		log.info("[${logId}] ${action} begins....")

		def forwardURI = request.forwardURI
		log.info("[${logId}] forwardURI: ${forwardURI}")

		def userAgent = request.getHeader("User-Agent")
		log.info("[${logId}] Agent: ${userAgent}")

		def referer = request.getHeader("Referer")
		log.info("[${logId}] Referer: ${referer}")

		def ip = request.getRemoteAddr()
		log.info("[${logId}] IP: ${ip}")

		String  responseStr    = ""
		Integer	responseStatus = 0

		switch (action) {
			case "POST":
				responseStr = doPost(logId)
				responseStatus = 201
				break

			case "PUT":
				responseStr = doPut(logId)
				responseStatus = 200
				break

			case "GET":
				responseStr = doGet(logId)
				responseStatus = 200
				break

			case "DELETE":
				responseStr = doDelete(logId)
				responseStatus = 200
				break
		}

		render(text: responseStr, status: responseStatus)

		long t2 = System.currentTimeMillis() - t1

		log.info("[${logId}] ${action} finished. Elapsed time: ${t2}ms")
	}

	String doPost(String logId) {
		log.info("[${logId}] POST.")

		// Mapea parametros de entrada
		Map inputParameters = MapUtil.requestBodyToMap(request)
		if (!inputParameters) {
			return "Not valid JSON"
		}

		String responseStr = inputParameters.toString()

		log.info("[${logId}] inputParameters: ${inputParameters}")

		return responseStr
	}

	String doPut(String logId) {
		log.info("[${logId}] PUT.")

		return "put"
	}

	String doGet(String logId) {
		log.info("[${logId}] GET.")

		return "get"
	}

	String doDelete(String logId) {
		log.info("[${logId}] DELETE.")

		return "delete"
	}
}
