package org.foa.api

class PingController {
	def defaultAction = "ping"

	def ping = {
		render(text: "pong", status: 200)
	}
}
